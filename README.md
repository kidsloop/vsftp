## VSFTPD
### 1.Deploy vsftpd:
- Go to https://gitlab.com/kidsloop/vsftp.git
- Make a branch from master, i.e.: add-config-xyz
- Edit yaml file as you need change, and commit [skip ci].
- Go to CICD => Run pipeline with variable vsftpd=true
### 2.Add New_user:
- Go to https://gitlab.com/kidsloop/vsftp.git
- Make a branch from master, i.e.: add-user-xyz
- Edit the variable USER&PASS in .gitlab-ci.yaml as you want and and commit [skip ci].
- Go to CICD => Run pipeline with variable add_user=true
